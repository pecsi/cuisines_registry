package de.quandoo.recruitment.registry.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Cuisine {

    private final String name;
    private ArrayList<Customer> customers;

    public static Map<String,Cuisine> allCuisines = new HashMap<>();

    private static final String ITALIAN = "italian";
    private static final String GERMAN = "german";
    private static final String FRENCH = "french";

    static {
        allCuisines.put(ITALIAN, Cuisine.createCuisine(ITALIAN));
        allCuisines.put(FRENCH, Cuisine.createCuisine(FRENCH));
        allCuisines.put(GERMAN, Cuisine.createCuisine(GERMAN));
    }

    private Cuisine(final String name) {
        this.name = name;
        this.customers = new ArrayList<>();

    }

    public static Cuisine createCuisine(final String name) {
        Cuisine c = Cuisine.allCuisines.get(name);
        if (c != null) {
            return c;
        }
        c = new Cuisine(name);
        return c;
    }

    public ArrayList<Customer> getCustomers() {
        return customers;
    }

    public void addCustomer(Customer customer) {
        if (customer != null && ! customers.contains(customer)) {
            customers.add(customer);
        }
    }

    public boolean isKnownCuisine() {
        return Cuisine.allCuisines.containsKey(name);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cuisine cuisine = (Cuisine) o;
        return name.equals(cuisine.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
