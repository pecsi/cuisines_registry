package de.quandoo.recruitment.registry.model;

import java.util.ArrayList;
import java.util.Objects;

public class Customer {
    private final String uuid;
    private ArrayList<Cuisine> cuisines;

    public Customer(final String uuid) {
        this.uuid = uuid;
        this.cuisines = new ArrayList<>();
    }

    public String getUuid() {
        return uuid;
    }

    public ArrayList<Cuisine> getCuisines() {
        return cuisines;
    }

    public void addCuisine(Cuisine cuisine) {

        if (cuisine != null && !cuisines.contains(cuisine)) {
            cuisines.add(cuisine);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return uuid.equals(customer.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
