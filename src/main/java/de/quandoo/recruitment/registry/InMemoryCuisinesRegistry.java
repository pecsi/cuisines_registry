package de.quandoo.recruitment.registry;

import com.google.common.primitives.Ints;
import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.ArrayList;
import java.util.List;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {


  @Override
  public void register(final Customer customer, final Cuisine cuisine) {

    if (cuisine != null) {
      if (!cuisine.isKnownCuisine()) {
        throw new IllegalArgumentException("Unknown cuisine, please reach johny@bookthattable.de to update the code");
      }
      if (customer != null) {
        cuisine.addCustomer(customer);
        customer.addCuisine(cuisine);
      }
    }
  }

  @Override
  public List<Customer> cuisineCustomers(final Cuisine cuisine) {
    if (cuisine != null && cuisine.isKnownCuisine())
      return cuisine.getCustomers();
    else
      return null;
  }

  @Override
  public List<Cuisine> customerCuisines(final Customer customer) {
    if (customer != null)
      return customer.getCuisines();
    else
      return null;
  }

  @Override
  public List<Cuisine> topCuisines(final int n) {
    List<Cuisine> cuisines = new ArrayList<>(Cuisine.allCuisines.values());
    cuisines.sort((o1, o2) -> Ints.compare(o2.getCustomers().size(), o1.getCustomers().size()));
    return cuisines.subList(0, n);
  }
}
