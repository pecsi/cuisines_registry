package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Test;

import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.*;

public class InMemoryCuisinesRegistryTest {

    private static final Cuisine CHINESE = Cuisine.createCuisine("chinese");
    private static final Cuisine FRENCH = Cuisine.createCuisine("french");
    private static final Cuisine GERMAN = Cuisine.createCuisine("german");
    private static final Cuisine ITALIAN = Cuisine.createCuisine("italian");
    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

    private static final Customer CUSTOMER_LOVING_FRENCH_CUISINE = new Customer("1");
    private static final Customer CUSTOMER_LOVING_FRENCH_AND_GERMAN_CUISINE = new Customer("2");
    private static final Customer CUSTOMER_LOVING_GERMAN_CUISINE = new Customer("3");
    private static final Customer CUSTOMER_LOVING_ITALIAN_CUISINE = new Customer("4");

    @Test
    public void whenCustomerRegistersForFrenchCuisineThenHeIsFrenchCuisineCustomer() {

        cuisinesRegistry.register(CUSTOMER_LOVING_FRENCH_CUISINE, FRENCH);
        Collection<Customer> frenchCuisineCustomers = cuisinesRegistry.cuisineCustomers(FRENCH);
        Collection<Customer> germanCuisineCustomers = cuisinesRegistry.cuisineCustomers(GERMAN);
        assertThat(frenchCuisineCustomers, hasItems(CUSTOMER_LOVING_FRENCH_CUISINE));
        assertTrue(germanCuisineCustomers.isEmpty());

    }

    @Test(expected = IllegalArgumentException.class)
    public void whenRegisterUnknownCuisineThrowException() {
        cuisinesRegistry.register(CUSTOMER_LOVING_FRENCH_CUISINE, CHINESE);
    }

    @Test
    public void whenRequestedCuisineCustomersWithNullCuisineReturnNull() {
        Collection<Customer> customers = cuisinesRegistry.cuisineCustomers(null);
        assertThat(customers,is(nullValue()));
    }


    @Test
    public void whenRequestedCuisineCustomersWithUnknownCuisineReturnNull() {
        Collection<Customer> customers = cuisinesRegistry.cuisineCustomers(CHINESE);
        assertThat(customers,is(nullValue()));
    }

    @Test
    public void whenRequestedCuisinesOfNullCustomerReturnNull() {
        List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(null);
        assertThat(cuisines,is(nullValue()));
    }

    @Test
    public void whenRequestedCuisinesOfFrenchLovingCustomerReturnFrenchDude() {
        cuisinesRegistry.register(CUSTOMER_LOVING_FRENCH_CUISINE, FRENCH);
        List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(CUSTOMER_LOVING_FRENCH_CUISINE);
        assertThat(cuisines,hasItems(FRENCH));
    }


    @Test
    public void whenFrenchCuisineIsMostPopularReturnFrenchCuisine() {
        cuisinesRegistry.register(CUSTOMER_LOVING_FRENCH_CUISINE, FRENCH);
        cuisinesRegistry.register(CUSTOMER_LOVING_FRENCH_AND_GERMAN_CUISINE, FRENCH);
        cuisinesRegistry.register(CUSTOMER_LOVING_FRENCH_AND_GERMAN_CUISINE, GERMAN);
        List<Cuisine> cuisines = cuisinesRegistry.topCuisines(2);
        assertThat(cuisines,hasItems(FRENCH, GERMAN));
        assertThat(FRENCH.getCustomers(), hasItems(CUSTOMER_LOVING_FRENCH_CUISINE, CUSTOMER_LOVING_FRENCH_AND_GERMAN_CUISINE));
        assertThat(GERMAN.getCustomers(), hasItems(CUSTOMER_LOVING_FRENCH_AND_GERMAN_CUISINE));

    }

    @Test
    public void whenGermanCuisineIsMostPopularReturnGermanCuisine() {
        cuisinesRegistry.register(CUSTOMER_LOVING_GERMAN_CUISINE, GERMAN);
        cuisinesRegistry.register(CUSTOMER_LOVING_FRENCH_AND_GERMAN_CUISINE, FRENCH);
        cuisinesRegistry.register(CUSTOMER_LOVING_FRENCH_AND_GERMAN_CUISINE, GERMAN);
        // pizza is not available right now so he goes for sausage
        cuisinesRegistry.register(CUSTOMER_LOVING_ITALIAN_CUISINE, GERMAN);
        List<Cuisine> cuisines = cuisinesRegistry.topCuisines(3);
        assertThat(cuisines,hasItems(GERMAN, FRENCH,ITALIAN));

    }


}